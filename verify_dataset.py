import argparse
import pygrib
import netCDF4
from pathlib import Path
import numpy as np
import sys
import logging
from typing import List, Dict, Any
from datetime import datetime, timedelta

#TODO Consider whitelist filtering of GRIB attributes
#TODO Add version information
#TODO Add GRIB grid support
#TODO Add GRIB globals support
#TODO Add GRIB consistency checks
#TODO Add name mapping
#TODO Add better rounding of numbers
#TODO Add support for <,>,<=,>= for comparisons
#TODO Convert to class
#TODO Consider xarrays

logger = logging.getLogger(__name__)

failed_checks = 0

def get_data(file_path):
    try:
        dataset = netCDF4.Dataset(file_path)
        file_dict = netcdf_data(dataset)
    except:
        try:
            dataset = pygrib.open(file_path)
            file_dict = grib_data(dataset)
        except:
            logger.error(f"Error parsing file: {file_path}")
            sys.exit(1)
    return file_dict

def args_to_dict(args: List[str]) -> Dict[str, Dict[str, str]]:
    """
    Converts a list of arguments into a dictionary.

    Each argument is expected to be in the format 'variable:attribute,value'.
    If ':' or ',' is not present in the argument, they are added at the end.
    The function returns a dictionary where each key is a 'variable' from the arguments,
    and the value is another dictionary with 'attribute' as key and 'value' as value.

    Args:
        args (List[str]): The arguments to be converted.

    Returns:
        Dict[str, Dict[str, str]]: The resulting dictionary.

    Raises:
        SystemExit: If an argument cannot be parsed.
    """
    # Initialize the result dictionary
    verify_dict = {}

    # Iterate over each argument
    for arg in args:
        try:
            # Ensure that ':' and ',' is in the argument
            if ':' not in arg:
                arg += ':'
            if ',' not in arg:
                arg += ','
            
            # Split the argument into variable, attribute, and value
            variable, rest = arg.split(':', 1)
            attribute, value = rest.split(',', 1)

            # Update dictionay
            if variable in verify_dict:
                verify_dict[variable][attribute] = value
            else:
                verify_dict[variable] = {attribute: value}
        except:
            # Log an error message if an argument cannot be parsed
            logger.error(f'Error: Could not parse verification: {arg}')
            sys.exit(1)
    
    return verify_dict

def grib_data(dataset):
    global failed_checks
    skip = [
        'values', 
        'codedValues', 
        'latLonValues', 
        'latitudes', 
        'longitudes', 
        'distinctLatitudes', 
        'distinctLongitudes',
        'pv',
        'validDate'
    ]
    grid_attrs = [
        'Ni', 
        'Nj', 
        'latitudeOfFirstGridPointInDegrees', 
        'longitudeOfFirstGridPointInDegrees', 
        'latitudeOfLastGridPointInDegrees', 
        'longitudeOfLastGridPointInDegrees', 
        'iDirectionIncrementInDegrees', 
        'jDirectionIncrementInDegrees', 
        'gridType',
        'latitudeOfSouthernPole',
        'latitudeOfSouthernPoleInDegrees',
        'longitudeOfSouthernPole',
        'longitudeOfSouthernPoleInDegrees',
        'latitudeOfLastGridPoint',
        'longitudeOfLastGridPoint',
        'iDirectionIncrement',
        'jDirectionIncrement',
        'gridDefinitionDescription',
        'gridDefinitionTemplateNumber',
        'latitudeOfFirstGridPoint',
        'longitudeOfFirstGridPoint'
    ]
    level_type_grib1 = {
        'heightAboveGround': '105',
        'heightAboveSea': '103',
        'meanSea': '102',
        'entireAtmosphere': '200',
        'surface': '001',
        'depthBelowSea': '160'
        }
    level_type_grib2 = {
        'heightAboveGround': '103',
        'heightAboveSea': '102',
        'meanSea': '101',
        'entireAtmosphere': '010',
        'surface': '001',
        'depthBelowSea': '160'
        }
    # Initialize dict
    dates = set()
    data = dict()
    for var in dataset:
        var.expand_grid(False)
        match var.GRIBEditionNumber:
            case 1:
                parameter_name = f'{var.indicatorOfParameter}.{level_type_grib1[var.typeOfLevel]}'
            case 2:
                parameter_name = f'{var.paramId}.{level_type_grib2[var.typeOfLevel]}'

        # Get grid from variable
        grid = {attr: getattr(var, attr) for attr in grid_attrs if attr in var.keys()}
        
        if var not in data:
            data[parameter_name] = {
                'maximum' : np.nan,
                'minimum' : np.nan
            }
        data[parameter_name].update({attr: getattr(var, attr) for attr in var.keys() if attr not in skip})

        # Locate values that should be masked
        values = var.values[:].ravel()
        values_idx = np.where(values != getattr(var, 'missingValue'))
        data[parameter_name].update({
            'maximum': max(var.maximum, values[values_idx].max()),
            'minimum': min(var.minimum, values[values_idx].min())
        })
        dates.add(getattr(var, 'analDate') + timedelta(hours=getattr(var, 'endStep')))
    data['time'] = {
        'size': len(dates),
        'maximum': np.max(list(dates)),
        'minimum': np.min(list(dates)),
        'shortName': 'time',
        }
    return data

def netcdf_data(dataset):
    # Initialize dict
    data = {'global': {}}
    data.update({dim : {} for dim in dataset.dimensions})
    data.update({var : {} for var in dataset.variables})

    # Add global attributes
    data['global'].update({attr : getattr(dataset, attr) for attr in dataset.ncattrs()})
    data['global']['shortName'] = 'global'

    # Add dimensions
    data.update({dim: {'size': len(dataset.dimensions[dim]), 'shortName': dim} for dim in dataset.dimensions.keys()})

    # Add variables
    for var in dataset.variables:
        v = dataset.variables[var]
        # Read all data ignoring valid_{range,min,max} and missing_value and _Fillvalue
        v.set_auto_mask(False)
        if var == 'time':
            t = netCDF4.num2date(v[:], v.units, calendar=getattr(v, 'calendar', 'standard'))
            data[var].update({'v_max_date': np.max(t[:]).isoformat(), 'v_min_date': np.min(t[:]).isoformat()})
        if v.shape and str(v.dtype) not in ['|S1']:
            # Locate values that should be masked
            values = v[:].ravel()
            values_idx = np.where((values != getattr(v, 'missing_value', np.nan)) & (values != getattr(v, '_FillValue', np.nan)) & (~np.isnan(values)))
            maximum = values[values_idx].max()
            minimum = values[values_idx].min()
        else:
            maximum = None
            minimum = None
        # Update dict
        data[var].update({attr: getattr(v, attr) for attr in v.ncattrs()})
        data[var].update({
            'datatype': str(v.dtype),
            'dimensions': ','.join(list(v.dimensions)),
            'maximum': maximum,
            'minimum': minimum
        })
        if not hasattr(data[var], 'shortName'):
            data[var].update({'shortName': var})
    return data


def verify(checks: Dict[str, Dict[str, Any]], data: Dict[str, Dict[str, Any]], verify_all: bool=False) -> int:
    """
    Verify the presence and values of specified attributes in a data dictionary.

    Parameters:
    checks (dict): A dictionary where each key is a variable name and the associated value is another dictionary of attributes and their expected values.
    data (dict): The data dictionary to be checked against the 'checks' dictionary.

    Returns:
    int: The number of checks that failed.
    """
    # Initialize a counter for the number of failed checks
    failed_checks: int = 0

    # If strict, check if there are un-verified variables
    if verify_all:
        for var in data:
            if var not in checks.keys() and var != 'time':
                logger.error(f'{var:>7}: Not verified')
                failed_checks += 1

    # Iterate over the checks dictionary
    for var, attrs in checks.items():
        # If the variable is not present in the data, log an error and increment the failed checks counter
        if var not in data:
            logger.error(f'{var}: Not found')
            failed_checks += 1
            continue

        if data[var]["shortName"] == var:
            var_string = var
        else:
            var_string = f'{var} ({data[var]["shortName"]})'
        # Iterate over the attributes dictionary
        for attr, expected_value in attrs.items():
            # If the attribute is an empty string, log it as found
            if attr == '':
                logger.info(f'{var_string:>18}: {attr} Found')
            # If the attribute is not present in the data, log an error and increment the failed checks counter
            elif attr not in data[var]:
                logger.error(f'{var_string:>18}: {attr} Not found')
                failed_checks += 1
            else:
                # Convert the expected value to the correct type for comparison
                if isinstance(data[var][attr], datetime):
                    expected_value = datetime.strptime(expected_value, '%Y-%m-%d %H:%M:%S')
                else:
                    expected_value = type(data[var][attr])(expected_value)
                # If the attribute is 'max' or 'min', use the operator module to compare values
                if attr in ['maximum', 'minimum']:
                    data_value = getattr(np, attr)(data[var][attr], expected_value)
                else:
                    data_value = data[var][attr]
                # If the expected value is an empty string, log it as found
                if expected_value == '':
                    logger.info(f'{var_string:>18}: {attr} Found')
                # If the data value matches the expected value, log the success
                elif data_value == expected_value:
                    logger.info(f'{var_string:>18}: {attr:>12} = {data[var][attr]}')
                # If the data value does not match, log an error and increment the failed checks counter
                else:
                    logger.error(f'{var_string:>18}: {attr:>12} = {data[var][attr]} not {expected_value}')
                    failed_checks += 1

    # Return the number of failed checks
    return failed_checks

def main():
    """
    Verify contents of GRIB and NetCDF files.

    This application is inteded for genereal verification of GRIB and NetCDF datasets. With the --verify argument one can specify any attribute present in the file.
    
    There are also a few virtual attributes, which can be verified.
     - size: for dimensions one can check that size.
     - maximum: for variables with numeric data on can check the maximum value is below threshold.
     - minimum: for variables with numeric data on can check the minimum value is below threshold.

    Example 1: verify_dataset --verify sst:units,PSU <input_file>

    Example 2: verify_dataset --verify latitude:size,1024 --verify longitude,size,512 <input_file>

    Example 3: verify_dataset --verify sst:maximum,42 --verify sst,minimum,0 <input_file>

    Args:
        input_file (str): Input file path
        verify (list): Verify specific contents, data:[attribute],[value]
        info (bool): Print file information
        show_loglevel (bool): Print the log levels
        show_timestamp (bool): Print timestamps
        loglevel (str): Set the log level (choices: DEBUG, INFO, WARNING, ERROR, CRITICAL)
    """
    parser = argparse.ArgumentParser(description='Verify contents of GRIB and NetCDF files.')
    parser.add_argument('input_file', type=str, help='Input file path')
    parser.add_argument('-v', '--verify', action='append', help='Verify specific contents, data:[attribute],[value]', default=[])
    parser.add_argument('--info', action='store_true', help='Print file information')
    parser.add_argument('--verify-all', action='store_true', help='Will only pass if all variables are verified')
    parser.add_argument('--show-loglevel', action='store_true', help='Print the log levels')
    parser.add_argument('--show-timestamp', action='store_true', help='Print timestamps')
    parser.add_argument('--loglevel', choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'], default='WARNING', help='Set the log level')

    args = parser.parse_args()

    # Set log level
    if args.info and args.loglevel != 'DEBUG':
        args.loglevel = 'INFO'
    logFormat = ''
    if args.show_timestamp:
        logFormat += '%(asctime)s '
    if args.show_loglevel:
        logFormat += '%(levelname)-8s '
    logFormat += '%(message)s'
    logging.basicConfig(stream=sys.stdout, level=args.loglevel, format=logFormat, datefmt='%Y-%m-%d %H:%M:%S')

    data = get_data(args.input_file)

    logger.info(f'Dataset: {Path(args.input_file).name}')

    if args.info:
        for var, attrs in data.items():
            logger.info(f"{var}:")
            for attr, value in attrs.items():
                logger.info(f"    {attr}: {value}")
            logger.info("")

    checks = args_to_dict(args.verify)

    global failed_checks
    failed_checks += verify(checks, data, verify_all=args.verify_all)
    if failed_checks:
        logger.error(f'File failed {failed_checks} check(s)!')
        sys.exit(1)
    else:
        logger.info(f'File passed all checks')
        sys.exit(0)

if __name__ == "__main__":
    main()
