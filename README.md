# GRIB and NetCDF Dataset Verifier

## Table of Contents
- [Description](#description)
- [Installation](#installation)
- [Usage](#usage)
- [Arguments](#arguments)
- [Logging](#logging)
- [Argument Table](#argument-table)
- [Examples](#examples)

## Description
This project provides a Python script designed to verify the contents of GRIB and NetCDF files. The script can check for the presence of specific attributes and values, and log detailed information about the datasets. It supports both GRIB (via `pygrib`) and NetCDF (via `netCDF4`) file formats, making it versatile for atmospheric and oceanographic data verification.

## Installation
To use this script, ensure you have Python installed along with the following dependencies:
- `argparse`
- `pygrib`
- `netCDF4`
- `numpy`

You can install these dependencies using pip:
```sh
pip install argparse pygrib netCDF4 numpy
```

## Usage
The script can be executed from the command line with various arguments to specify the file and the checks to perform.

## Arguments
- `input_file` (str): Path to the input file.
- `-v, --verify` (list): Verify specific contents in the format `variable:attribute,value`.
- `--info` (bool): Print file information.
- `--verify-all` (bool): Only pass if all variables are verified.
- `--show-loglevel` (bool): Print the log levels.
- `--show-timestamp` (bool): Print timestamps.
- `--loglevel` (str): Set the log level (choices: DEBUG, INFO, WARNING, ERROR, CRITICAL). Default is `WARNING`.

## Logging
The script supports customizable logging formats, including log levels and timestamps. Use the `--show-loglevel` and `--show-timestamp` flags to include these details in the logs.

### Example
To run the script with detailed logging and timestamps:
```sh
python verify_dataset.py --verify sst:units,PSU <input_file> --show-loglevel --show-timestamp --loglevel DEBUG
```

This will provide detailed debugging information with timestamps to help trace the execution and verification process.

## Argument Table

| Argument               | Description                                                                                              |
|------------------------|----------------------------------------------------------------------------------------------------------|
| `input_file`           | Path to the input file.                                                                                  |
| `-v, --verify`         | Verify specific contents in the format `variable:attribute,value`.                                        |
| `--info`               | Print file information.                                                                                  |
| `--verify-all`         | Only pass if all variables are verified.                                                                 |
| `--show-loglevel`      | Print the log levels.                                                                                    |
| `--show-timestamp`     | Print timestamps.                                                                                        |
| `--loglevel`           | Set the log level (choices: DEBUG, INFO, WARNING, ERROR, CRITICAL). Default is `WARNING`.                |

This table summarizes the command-line arguments available for the script, providing a quick reference for users.

## Examples

### Example 1
Verify that the 'sst' variable has units 'PSU':
```sh
python verify_dataset.py --verify sst:units,PSU <input_file>
```

### Example 2
Verify the size of the 'latitude' and 'longitude' dimensions:
```sh
python verify_dataset.py --verify latitude:size,1024 --verify longitude:size,512 <input_file>
```

### Example 3
Verify the maximum and minimum values of the 'sst' variable:
```sh
python verify_dataset.py --verify sst:maximum,42 --verify sst:minimum,0 <input_file>
```